# R project template

## Description

The objective of this project is to provide standardised templates for R scripts, R Markdown and/or Quarto documents, and to create the standard set of directories commonly used in our workflow. 
It does so by copying across these templates from this project folder to the directory specified in the *initialise.R* script.

The suggested workflow is to:
1.  Create your new project on GitLab/GitHub (remember to use a concise but descriptive name using underscores and lower case). 
Choose to include the default readme file, but it will be overwritten in the steps that follow.  
2.  From within Rstudio, clone the project from the online repository to your chosen parent folder on your computer.  
      - Copy the relevant '*Clone with ...*' URL from the project page on GitLab/GitHub.  
      - In RStudio, create a new project (under *File* menu) and select *Version Control* and then *Git* to paste the copied URL.  
      - Once you have pasted the URL into the relevant box, check that the 'Project directory name' uses underscores, not dashes and check if the project is being created in your preferred folder (if not, use the browse button to select the appropriate parent folder).  
3.  Close the new project and switch to this (r_project_template) project.
4.  Open the *initialise.R* script; update its *target_path* to point to the project folder created in step 2 above and run (source) it.  
5.  Close this project and re-open the newly created project, which should contain the template folders and documents.
6.  Make your life easier, by saving those templates as your standard templates on Rstudio, so that each time you click on *File* -> *New File*, the same template structure will be created:
      - to do so, check the second (and third) answer on this [stackoveflow page](https://stackoverflow.com/questions/35158708/how-to-set-default-template-for-new-r-files-in-rstudio) for instructions and [here](https://docs.posit.co/ide/server-pro/1.3.399-1/r-sessions.html#default-document-templates) for the default file names to use. 
      - alternatively, you can set up RStudio to make use of custom [snippets](https://timfarewell.co.uk/my-r-script-header-template/).
      While you're there, see some of the other useful snippets already available in RStudio.
7.  Delete the unused template files (and folders if they are not expected to be used) to 'clean up' before submitting or publishing the project.

## Details

The default .gitignore file copied across purposefully specifies the 'data' directory, as most data we use may not be shared publicly.
It also specifies that '.Renviron' should be ignored, in case it contains passwords.
**Remember to update the .gitignore file to suit your needs and the sensitivity of data or outputs created.** 
Remove the 'data' directory if you want it to be version controlled (and available to collaborators or public, depending on the private/public setting on the online repository!).

## Notes

The reason for this (cumbersome) workflow to create custom templates, is that [GitLab custom project templates](https://docs.gitlab.com/ee/administration/custom_project_templates.html) are available only on the paid tier!
There might be an alternative way via [RStudio Project Templates](https://rstudio.github.io/rstudio-extensions/rstudio_project_templates.html), but I couldn't figure it out and leave it to someone with more time on their hands, if they'd like to.

## Acknowledgements

